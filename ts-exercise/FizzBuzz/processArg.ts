class ProcessArg {
  private x: string;
  private y: string;
  constructor(x: string, y: string) {
    this.x = x;
    this.y = y;
  }

  fizzBuzz(fileContent: string[]) {
    for (let line of fileContent) {
      let pair: string[] = line.split(",");
      let firstDigit: number = Number(pair[0]);
      let secondDigit: number = Number(pair[1]);
      this.processTwoNumber(firstDigit, secondDigit);
    }
  }

  private processTwoNumber(firstDigit: number, secondDigit: number) {
    let content: string = "";

    for (let i = firstDigit; i <= secondDigit; i++) {
      if (i % 3 == 0 && i && i % 5 == 0) {
        content = content.concat(this.x + this.y + " ");
      } else if (i % 3 == 0) {
        content = content.concat(this.x + " ");
      } else if (i % 5 == 0) {
        content = content.concat(this.y + " ");
      } else {
        content = content.concat(i + " ");
      }
    }
    console.log(content);
  }
}

export { ProcessArg };
