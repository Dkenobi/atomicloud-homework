import * as fs from "fs";
import { ProcessArg } from "./processArg";
import { ErrorMessage } from "./errorMessage";

let args = process.argv.splice(2);
if (args.length == 3) {
  const fileName = args[0];
  const fileContent = fs.readFileSync(fileName, "utf8").split("\n");
  const processArg: ProcessArg = new ProcessArg(args[1], args[2]);
  processArg.fizzBuzz(fileContent);
} else {
  new ErrorMessage().errMsg();
}
