import * as fs from "fs";

let args = process.argv.splice(2);
processArg(args);

function processArg(args: Array<any>) {
  if (args.length == 2) {
    if (Number(args[1])) {
      const fileContent = fs.readFileSync(args[0], "utf8").split(" ");
      let finalContent = wrapText(fileContent, args[1]);
      fs.writeFileSync(args[0], finalContent);
    } else {
      errMsg();
    }
  } else {
    errMsg();
  }
}

function wrapText(fileContent: String[], wordsPerLine: number): String {
  let content: String = "";
  let count: number = 0;
  for (let line of fileContent) {
    if (count == wordsPerLine) {
      content = content.concat("\n");
      count = 0;
    }
    content = content.concat(line + " ");
    count++;
  }
  return content;
}

function errMsg() {
  console.log(
    'Please specify the arg in the following format "ts-node wrap.ts <file_name> <word_per_line>" ' +
      "\ne.g. ts-node wrap.ts file.txt 3"
  );
}
