import * as fs from "fs";

const dbFilePath = "db.txt";
let args = process.argv.splice(2);
let map = new Map();
let db: String[] = fs
  .readFileSync(dbFilePath, "utf8")
  .trim()
  .split("\n");

if (db[0] != "") {
  for (let line of db) {
    let keyPair: String[] = line.split(",");
    map.set(keyPair[0], keyPair[1]);
  }
}

switch (args[0]) {
  case "load":
    if (map.has(args[1])) {
      console.log(map.get(args[1]));
    } else {
      console.log("NULL");
    }
    break;

  case "save":
    map.set(args[1], args.slice(2).join(" "));
    fs.writeFileSync(dbFilePath, saveToString(map));
    break;

  case "delete":
    map.delete(args[1]);
    fs.writeFileSync(dbFilePath, saveToString(map));

  default:
    break;
}

function saveToString(map: Map<String, String>): String {
  let saveData: String = "";

  map.forEach((value: String, key: String) => {
    saveData = saveData.concat(key + "," + value + "\n");
  });

  return saveData;
}
