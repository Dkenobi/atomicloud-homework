﻿using System;
using System.IO;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            String filePath = args[0];
            int wordLimit = int.Parse(args[1]);
            var fileContent = File.ReadAllText(filePath).Split(" ");

            Program p = new Program();
            p.wrapText(filePath, fileContent, wordLimit);
        }

        void wrapText(String filePath, String[] fileContent, int wordLimit)
        {
            String content = "";
            int count = 0;
            foreach (String word in fileContent)
            {
                if (count == wordLimit)
                {
                    content += Environment.NewLine;
                    count = 0;
                }
                content += word + " ";
                count++;
            }
            File.WriteAllText(filePath, content);
        }

    }
}

