﻿using System;
using System.IO;

namespace FizzBuzz
{
    public class FizzBuzzObj
    {
        private string x;
        private string y;

        public FizzBuzzObj(string x, string y)
        {
            this.x = x;
            this.y = y;
        }

        public String fizzBuzzCal(String[] filecontent)
        {
            String content = "";

            foreach (String line in filecontent)
            {
                string[] pair = line.Split(",");
                int firstDigit = int.Parse(pair[0]);
                int secondDigit = int.Parse(pair[1]);

                for (int i = firstDigit; i < secondDigit; i++)
                {
                    if (i % 3 == 0)
                    {
                        content += this.x + " ";
                    }
                    else if (i % 5 == 0)
                    {
                        content += this.y + " ";
                    }
                    else
                    {
                        content += i + " ";
                    }
                }
                content += Environment.NewLine;
            }
            return content.Trim();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            String filePath = args[0];
            String x = args[1];
            String y = args[2];
            var fileContent = File.ReadAllText(filePath).Split("\n");

            FizzBuzzObj f = new FizzBuzzObj(x, y);
            Console.WriteLine(f.fizzBuzzCal(fileContent));
        }
    }
}

